from django import forms

from orders.models import Order


class OrderForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        # 'placeholder': Order.initiator.first_name,
    }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        # 'placeholder': Order.initiator.last_name
    }))
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        # 'placeholder': Order.initiator.email,
    }))
    address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        # 'placeholder': 'Россия, Москва, ул. Мира, дом 6',
    }))

    class Meta:
        model = Order
        fields = ('first_name', 'last_name', 'email', 'address')
